# -*- coding: utf-8 -*-
# Copyright (c) 2015, Revant Nandgaonkar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class GSTAPISettings(Document):
	pass

@frappe.whitelist()
def generate_app_key():
	apikey = frappe.generate_hash(length=32)
	return apikey
