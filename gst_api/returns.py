import frappe
import cryptography
import base64
import requests
import json
import urllib

from frappe import _
from cryptography import x509
from cryptography.hazmat.primitives import hashes, hmac
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from .authenticate import encrypt_data, decrypt_data, encrypt_using_public_key, decode_json_response
from .authenticate import get_app_key, get_gst_user, get_gst_settings, request_txn, mac256

def save_gstr1_data(sek=None, auth_token=None):
	if not sek:
		print "Provide sek"
		return
	
	if not auth_token:
		print "Provide auth-token"
		return

	ek = decrypt_data(sek, get_app_key())
	
	save_data = {  
		"gstin":get_gst_user().gstin,
		"fp":get_return_period(),
		"gt":3782969.01,
		"b2b":[  
			{  
				"ctin":"27GSPMH0012G1Z4",
				"inv":[  
					{  
						"inum":"SFEB1231222",
						"idt":"24-11-2016",
						"val":729248.16,
						"pos":"06",
						"rchrg":"N",
						"prs":"Y",
						"etin":"01AABCE5507R1Z4",
						"itms":[  
							{  
								"num":1,
								"itm_det":{  
									"ty":"G",
									"hsn_sc":"G1221",
									"txval":10000,
									"irt":3,
									"iamt":833.33,
									"crt":4,
									"camt":500,
									"srt":5,
									"samt":900,
									"csrt":2,
									"csamt":500
								}
							}
						]
					}
				]
			}
		],
		"b2cl":[  
			{  
				"state_cd":"29",
				"inv":[  
					{  
						"cname":"R_Glasswork Enterprise",
						"inum":"B12354112",
						"idt":"14-11-2016",
						"val":1000.03,
						"pos":"06",
						"prs":"Y",
						"etin":"27AHQPA8875L1ZU",
						"itms":[  
							{  
								"num":1,
								"itm_det":{  
									"ty":"S",
									"hsn_sc":"S249",
									"txval":10000,
									"irt":3,
									"iamt":833.33,
									"csrt":2,
									"csamt":500
								}
							}
						]
					}
				]
			}
		],
		"b2cs":[  
			{  
				"state_cd":"29",
				"ty":"G",
				"hsn_sc":"G2469",
				"txval":10000,
				"irt":3,
				"iamt":500,
				"crt":4,
				"camt":500,
				"srt":5,
				"samt":900,
				"csrt":3,
				"csamt":833,
				"prs":"Y",
				"etin":"20ABCDE7588L1ZJ",
				"typ":"E"
			}
		],
		"at":[  
			{  
				"typ":"B2B",
				"cpty":"12DEFPS5555D1Z2",
				"state_cd":"12",
				"doc_num":"100001",
				"doc_dt":"24-11-2016",
				"ad_amt":100,
				"itms":[  
					{  
						"ty":"S",
						"hsn_sc":"S9043",
						"irt":3,
						"iamt":833.33,
						"crt":4,
						"camt":500,
						"srt":5,
						"samt":900,
						"csrt":2,
						"csamt":500
					}
				]
			}
		],
		"exp":[  
			{  
				"ex_tp":"WPAY",
				"inv":[  
					{  
						"inum":"81578978112",
						"idt":"12-11-2016",
						"val":995048.36,
						"sbpcode":"ASB9950",
						"sbnum":84298,
						"sbdt":"04-10-2016",
						"prs":"Y",
						"itms":[  
							{  
								"ty":"G",
								"hsn_sc":"G9207",
								"txval":10000,
								"irt":3,
								"iamt":833.33,
								"crt":4,
								"camt":500,
								"srt":5,
								"samt":900,
								"csrt":2,
								"csamt":500
							}
						]
					}
				]
			}
		],
		"nil":[  
			{  
				"g":[  
					{  
						"sply_ty":"INTRB2B",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					},
					{  
						"sply_ty":"INTRB2C",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					},
					{  
						"sply_ty":"INTRAB2B",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					},
					{  
						"sply_ty":"INTRAB2C",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					}
				],
				"s":[  
					{  
						"sply_ty":"INTRB2B",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					},
					{  
						"sply_ty":"INTRB2C",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					},
					{  
						"sply_ty":"INTRAB2B",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					},
					{  
						"sply_ty":"INTRAB2C",
						"expt_amt":123.45,
						"nil_amt":1470.85,
						"ngsup_amt":1258.5
					}
				]
			}
		],
		"hsnSac":[  
			{  
				"data":[  
					{  
						"num":1,
						"ty":"G",
						"hsn_sc":"1009",
						"txval":10.23,
						"irt":12.52,
						"iamt":14.52,
						"crt":78.52,
						"camt":78.52,
						"srt":12.34,
						"samt":12.9,
						"csrt":2,
						"csamt":500,
						"desc":"Goods Description",
						"uqc":"1",
						"qty":2.05,
						"sply_ty":"INTRB2B"
					}
				]
			}
		],
		"txpd":[  
			{  
				"typ":"B2B",
				"cpty":"29AABFZ0458N1ZL",
				"inum":"SFEB1231112",
				"idt":"20-11-2016",
				"doc_num":"100001",
				"doc_dt":"23-09-2016",
				"irt":3,
				"iamt":833.33,
				"crt":4,
				"camt":500,
				"srt":5,
				"samt":900,
				"csrt":2,
				"csamt":500
			}
		]
	}

	headers = get_returns_headers(auth_token)
	encoded_data = encrypt_data(json.dumps(save_data),ek)

	payload = {
		"action":"RETSAVE",
		"data":encoded_data,
		"hmac":mac256(json.dumps(save_data),ek)
	}

	url="http://devapi.gstsystem.co.in/taxpayerapi/v0.2/returns/gstr1"

	return request_txn(method="PUT", url=url, payload=payload, headers=headers, add_txn_header=True)

def submit_gstr1_data(sek=None, auth_token=None):
	if not sek:
		print "Provide sek"
		return
	
	if not auth_token:
		print "Provide auth-token"
		return

	ek = decrypt_data(sek, get_app_key())
	
	save_data = {  
		"gstin":get_gst_user().gstin,
		"ret_period":get_return_period(),
	}

	headers = get_returns_headers(auth_token)
	encoded_data = encrypt_data(json.dumps(save_data),ek)

	payload = {
		"action":"RETSUBMIT",
		"data":encoded_data,
		"hmac":mac256(json.dumps(save_data),ek)
	}

	url="http://devapi.gstsystem.co.in/taxpayerapi/v0.2/returns/gstr1"

	return request_txn(method="PUT", url=url, payload=payload, headers=headers, add_txn_header=True)

def get_gstr1_b2b_data(url=None, sek=None, auth_token=None, ctin=None, from_time=None, action_required='Y'):

	ctin = "27GSPMH0012G1Z4" if not ctin else ctin
	from_time = "04-02-2017" if not from_time else from_time # DD-MM-YYYY

	if not sek:
		print "Provide sek"
		return
	
	if not auth_token:
		print "Provide auth-token"
		return

	if not url:
		url="http://devapi.gstsystem.co.in/taxpayerapi/v0.2/returns/gstr1"
	url += "?" + "action=B2B" + \
			"&action_required=" + action_required + \
			"&gstin=" + get_gst_user().gstin + \
			"&ret_period=" + get_return_period() + \
			"&ctin=" + ctin + \
			"&from_time=" + from_time

	headers = get_returns_headers(auth_token)

	return request_txn(method="GET", url=url, headers=headers, add_txn_header=True)

def get_gstr1_summary(url=None, sek=None, auth_token=None):

	if not sek:
		print "Provide sek"
		return
	
	if not auth_token:
		print "Provide auth-token"
		return

	if not url:
		url="http://devapi.gstsystem.co.in/taxpayerapi/v0.2/returns/gstr1"
	url += "?" + "action=RETSUM" + \
			"&gstin=" + get_gst_user().gstin + \
			"&ret_period=" + get_return_period()

	headers = get_returns_headers(auth_token)

	return request_txn(method="GET", url=url, headers=headers, add_txn_header=True)

def file_gstr1(sek=None, auth_token=None):
	if not sek:
		print "Provide sek"
		return
	
	if not auth_token:
		print "Provide auth-token"
		return

	ek = decrypt_data(sek, get_app_key())
	
	save_data = {
		"gstin": "37ABCDE9552F3Z4",
		"ret_period": "072016",
		"chksum": "AflJufPlFStqKBZ",
		"sec_sum": [
			{
				"sec_nm": "b2b",
				"chksum": "AflJufPlFStqKBZ",
				"ttl_rec": 10,
				"ttl_val": 12345,
				"ttl_igst": 124.99,
				"ttl_cgst": 3423,
				"ttl_sgst": 5589.87,
				"ttl_cess": 3423,
				"ttl_tax": 1234,
				"cpty_sum": [
					{
						"ctin": "20GRRHF2562D3A3",
						"chksum": "AflJufPlFStqKBZ",
						"ttl_rec": 10,
						"ttl_val": 12345,
						"ttl_igst": 124.99,
						"ttl_cgst": 3423,
						"ttl_sgst": 5589.87,
						"ttl_cess": 3423,
						"ttl_tax": 1234
					}
				]
			}
		]
	}

	headers = get_returns_headers(auth_token)
	encoded_data = encrypt_data(json.dumps(save_data),ek)

	payload = {
		"action":"RETFILE",
		"data":encoded_data,
		"hmac":mac256(json.dumps(save_data),ek)
	}

	url="http://devapi.gstsystem.co.in/taxpayerapi/v0.2/returns/gstr1"

	return request_txn(method="PUT", url=url, payload=payload, headers=headers, add_txn_header=True)

def get_returns_headers(token=None):
	if not token:
		print "token required"
		return

	headers = {
		'auth-token': token,
		'clientid': get_gst_settings().clientid,
		'state-cd': get_gst_user().state_code,
		'username': get_gst_user().gsp_user,
		'content-type': "application/json",
		'gstin': get_gst_user().gstin,
		'ret_period': get_return_period(),
		'client-secret':get_gst_settings().client_secret,
	}

	try:
		headers['ip-usr'] = frappe.get_request_header('REMOTE_ADDR')
	except Exception as e:
		headers['ip-usr'] = json.loads(urllib.urlopen("http://ip.jsontest.com/").read())["ip"]

	return headers

def get_return_period(date=None):
	out = ""
	if not date:
		now = frappe.utils.datetime.datetime.now()
		out = now.strftime("%m%Y") # works with current month/year
	else:
		out = date

	return out

def get_cash_ledger_details(sek=None, auth_token=None, ctin=None, fr_dt=None, to_dt=None, action_required='Y'):
	if not fr_dt or not to_dt:
		print ("Provide frm and to dt")
	if not sek:
		print "Provide sek"
		return

	if not auth_token:
		print "Provide auth-token"
		return

	url="http://devapi.gstsystem.co.in/taxpayerapi/v0.2/ledgers"
	#url = "https://httpbin.org/get" 
	url += "?" + "fr_dt=" + fr_dt + \
			"&gstin=" + get_gst_user().gstin + \
			"&to_dt=" + to_dt

	headers = get_returns_headers(auth_token)

	return request_txn(method="GET", url=url, headers=headers)
